﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SphereMover : MonoBehaviour {
	public bool tak1;
	public bool tak2;
	public bool kitkat=true;
	public GameObject tapForRestart;
	public GameObject tapForRestart1;
	public int sphereSpeedPassing;
	public float sphereSpeedPingPong;
	public CameraController cameraController1;
	public float sphereRotationSpeed;
	public CircleController circleController;
	public bool conta;
	public Button button;
	// Use this for initialization
	void Start () {
		cameraController1 = GameObject.Find ("Main Camera").GetComponent<CameraController> ();

		conta = true;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.forward * sphereRotationSpeed);
		if (kitkat) {
			transform.position = new Vector3 (Mathf.PingPong (Time.time * sphereSpeedPingPong, 14f) - 33.5f, transform.position.y, transform.position.z);
		}
		#if UNITY_EDITOR
		if(EventSystem.current.IsPointerOverGameObject())
		{
			return;
		}

		else if (Input.GetMouseButtonDown(0))
		{
			
		/*	RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit)){
			if (hit.transform.tag == "15") {


					return;

			}

		}*/
			
				kitkat=false;

		}
		#endif

		#if UNITY_ANDROID || UNITY_IOS

		if(Input.touchCount > 0 && Input.GetTouch(0).phase==TouchPhase.Began)
		{
			foreach (Touch touch in Input.touches)
			{
		
				int id = touch.fingerId;
			if (EventSystem.current.IsPointerOverGameObject(id))
		{

				return;
		}
			else
			{
				kitkat=false;
			}
		}
		}
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();

		}
		#endif
		if(kitkat==false)
			transform.Translate (Vector3.forward * Time.deltaTime * sphereSpeedPassing);
		}


	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.name == "cerchio1" || other.gameObject.name=="cerchio1(Clone)")
		{
			
			cameraController1.numberTimesToPassCamera++;
		}

		if (other.gameObject.name == "ColliderObjectMesh") {
			Debug.Log ("Game Over!");
			tapForRestart.SetActive (true);
			tapForRestart1.SetActive (true);
			conta = false;
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.name == "ColliderForCameraStop") {
			//cameraController1.numberTimesToPassCamera--;
		}
	}

}
