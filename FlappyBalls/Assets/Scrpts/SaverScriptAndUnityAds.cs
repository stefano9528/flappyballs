﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SaverScriptAndUnityAds : MonoBehaviour {

	public static SaverScriptAndUnityAds saverScript;
	public int numberOfSlowBall;
	public int liveRecord;
	private int savedRecord;
	private bool saveInPref;
	private string savedInPrefString;
	public int loadedFromPrefINT;
	public string loadedFromPrefString;
	public EncryptingScript encryptingScript;
	public Text recordText;
	public bool loadedfromprefbool;
	public string gia="0";
	public Text slowballText;
	public string numberOfSlowballString;
	public bool DoIt=false;

	void Awake()
	{
		if (saverScript == null) {
			DontDestroyOnLoad (gameObject);
			saverScript = this;
		} else if (saverScript != this)
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {

		encryptingScript = GameObject.Find ("EncrypterObject").GetComponent<EncryptingScript> ();

		loadedFromPrefString=encryptingScript.Decrypt(PlayerPrefs.GetString("savedRecordInPrefString"));
		loadedFromPrefINT=System.Int32.Parse(loadedFromPrefString);
		savedRecord=loadedFromPrefINT;
		numberOfSlowballString=encryptingScript.Decrypt(PlayerPrefs.GetString("savedSlowBall"));
		numberOfSlowBall=System.Int32.Parse(numberOfSlowballString);
	}



	// Update is called once per frame
	void Update () {
	
		if (liveRecord > savedRecord) 
		{
			saveInPref=true;
			savedRecord = liveRecord;

		}


		if(SceneManager.GetActiveScene().name=="Menu" && saveInPref==true)
		{
			savedInPrefString=savedRecord.ToString();
			PlayerPrefs.SetString("savedRecordInPrefString",encryptingScript.Encrypt(savedInPrefString));

			loadedfromprefbool = true;
			saveInPref=false;
		}
		if(SceneManager.GetActiveScene().name=="Menu" && loadedfromprefbool==true)
		{
			loadedFromPrefString=encryptingScript.Decrypt(PlayerPrefs.GetString("savedRecordInPrefString"));
			loadedFromPrefINT=System.Int32.Parse(loadedFromPrefString);
			savedRecord=loadedFromPrefINT;

			loadedfromprefbool=false;
		}
		if(SceneManager.GetActiveScene().name=="Menu")
		{
			recordText = GameObject.Find ("RecordText").GetComponent<Text> ();
			recordText.text="Record: "+loadedFromPrefString;
			slowballText = GameObject.Find ("SlowBallText").GetComponent<Text> ();
			slowballText.text = "SlowBall: " + numberOfSlowBall;
		}

		if(SceneManager.GetActiveScene().name=="Menu" && DoIt==true)
		{

		numberOfSlowballString = numberOfSlowBall.ToString ();
		PlayerPrefs.SetString("savedSlowBall",encryptingScript.Encrypt(numberOfSlowballString));
			DoIt = false;
	}
	}

	public void ShowRewardedAds()
	{
		if (Advertisement.IsReady ("rewardedVideo")) 
		{
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show ("rewardedVideo", options);
		}
	}

	private void HandleShowResult (ShowResult result)
	{
		switch (result) 
		{
		case ShowResult.Finished:
			Debug.Log ("Ad Finished");

			numberOfSlowBall++;
			numberOfSlowballString = numberOfSlowBall.ToString ();
			PlayerPrefs.SetString("savedSlowBall",encryptingScript.Encrypt(numberOfSlowballString));
			break;


		case ShowResult.Skipped:
			Debug.Log ("Ad Skipped");



			break;

		case ShowResult.Failed:
			Debug.Log ("Ad Failded");


			break;
		}
	}

}
