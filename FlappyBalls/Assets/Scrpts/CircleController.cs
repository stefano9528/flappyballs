﻿using UnityEngine;
using System.Collections;

public class CircleController : MonoBehaviour {

	public GameObject cerchiodaspawn;
	public GameController gameController;
	public CameraController cameraController;
	public SphereMover sphereMover;
	public SaverScriptAndUnityAds saverScriptAndUnityAds;

	// Use this for initialization
	void Start () {
		saverScriptAndUnityAds = GameObject.Find ("SaverAndUnityAds").GetComponent<SaverScriptAndUnityAds> ();

	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.back );
	}

	void OnTriggerExit (Collider other)
	{
		
		Debug.Log ("YEAH");
		gameController=GameObject.Find ("GameController").GetComponent<GameController> ();
		cameraController = GameObject.Find ("Main Camera").GetComponent<CameraController> ();
		sphereMover = GameObject.Find ("Sphere").GetComponent<SphereMover> ();
		if (sphereMover.conta == true) {
			saverScriptAndUnityAds.liveRecord++;
		}
		gameController.Insta = true;
		//cameraController.insta2 = true;


		StartCoroutine("WaitForNextTap");
	}
	IEnumerator WaitForNextTap ()
	{
		yield return new WaitForSeconds (0.01f);
		sphereMover.kitkat = true;
	}
}
