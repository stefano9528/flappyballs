﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using GoogleMobileAds;

public class AdmobBannerScript : MonoBehaviour {

	public static AdmobBannerScript admobBannerScript;
	public bool zanzibarbat=true;
	public EncryptingScript encryptingScript3;
	private string oppa;
	// Use this for initialization
	void Awake()
	{
		if (admobBannerScript == null) {
			DontDestroyOnLoad (gameObject);
			admobBannerScript = this;
		} else if (admobBannerScript != this)
			Destroy (gameObject);
		
	}
	void Start () {
		encryptingScript3 = GameObject.Find ("EncrypterObject").GetComponent<EncryptingScript> ();
		oppa = encryptingScript3.Decrypt (PlayerPrefs.GetString ("RecordSaved"));
		Debug.Log (oppa);
		if (oppa == "jimny") {
			zanzibarbat = false;
		}
		if (zanzibarbat==true) {
			Debug.Log ("zar");
		//	RequestBanner ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

		
	private void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-9745961044622708/6558083475";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		BannerView bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		}

}
