﻿using UnityEngine;
using System.Collections;

public class BackGroundRotator : MonoBehaviour {

	public Sprite ToRender;
	public Sprite BackGround1;
	public Sprite BackGround2;
	public Sprite BackGround3;
	public Sprite BackGround4;
	public Sprite BackGround5;

	private SpriteRenderer ToRend;
	public int backGroundRotationSpeed;

	// Use this for initialization
	void Start () {
		ToRend = GetComponent<SpriteRenderer>();
		StartCoroutine ("Fader");
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.forward * backGroundRotationSpeed);
		if (ToRend.sprite == BackGround1 || ToRend.sprite==BackGround2 || ToRend.sprite==ToRender) 
		{
			transform.localScale = new Vector3(5.5f,6.5f,1f);
		}
		if (ToRend.sprite == BackGround3 || ToRend.sprite==BackGround4 || ToRend.sprite==BackGround5) 
		{
			transform.localScale = new Vector3(7.5f,10f,1f);
		}

	}
		IEnumerator Fader(){
			while(true){
			yield return new WaitForSeconds (5f);
		for (float f = 1f; f >= 0; f -= 0.085f) {
			ToRend.sprite=ToRender;
			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		for (float f = 0f; f <= 1; f += 0.085f) {
			ToRend.sprite=BackGround1;
			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		yield return new WaitForSeconds(4);
		for (float f = 1f; f >= 0; f -= 0.085f) {

			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		for (float f = 0f; f <= 1; f += 0.085f) {
			ToRend.sprite=BackGround2;
			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		yield return new WaitForSeconds(4);

		for (float f = 1f; f >= 0; f -= 0.085f) {

			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		for (float f = 0f; f <= 1; f += 0.085f) {
			ToRend.sprite=BackGround3;
			Color c = ToRend.color;
			c.a = f;
			ToRend.color = c;
			yield return null;
		}
		yield return new WaitForSeconds(4);

			for (float f = 1f; f >= 0; f -= 0.085f) {

				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}
			for (float f = 0f; f <= 1; f += 0.085f) {
				ToRend.sprite=BackGround4;
				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}

			yield return new WaitForSeconds(4);

			for (float f = 1f; f >= 0; f -= 0.085f) {

				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}
			for (float f = 0f; f <= 1; f += 0.085f) {
				ToRend.sprite=BackGround5;
				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}

			yield return new WaitForSeconds(4);

			for (float f = 1f; f >= 0; f -= 0.085f) {

				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}
			for (float f = 0f; f <= 1; f += 0.085f) {
				ToRend.sprite=ToRender;
				Color c = ToRend.color;
				c.a = f;
				ToRend.color = c;
				yield return null;
			}

	}
}
}