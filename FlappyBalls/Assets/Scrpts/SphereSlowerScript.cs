﻿using UnityEngine;
using System.Collections;

public class SphereSlowerScript : MonoBehaviour {
	public static SphereSlowerScript sphereSlowerScript;
	public SphereMover sphereMover;
	public SaverScriptAndUnityAds saverScriptAndUnityAds;
	// Use this for initialization

	void Start () {
		saverScriptAndUnityAds = GameObject.Find ("SaverAndUnityAds").GetComponent<SaverScriptAndUnityAds> ();
	}
	
	// Update is called once per frame
	void Update () {
	


	}
	public void SlowerButton()
	{
		sphereMover = GameObject.Find ("Sphere").GetComponent<SphereMover> ();
		if (saverScriptAndUnityAds.numberOfSlowBall > 0) {
			saverScriptAndUnityAds.numberOfSlowBall--;
			StartCoroutine ("Slower");
			saverScriptAndUnityAds.DoIt = true;
		}
	}

	IEnumerator Slower ()
	{
		sphereMover.sphereSpeedPingPong = 8f;
		yield return new WaitForSeconds (5f);
			sphereMover.sphereSpeedPingPong=20f;
	}
}
