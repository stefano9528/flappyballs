﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	public GameObject cerchiodaspawn;
	public bool Insta=false;
	public int zAxis;
	public Text passedText;
	public int PassedInt;
	public int moreZCircle;
	public GameObject sphereMover2;	
	public GameObject cameraController2;
	public SphereMover sphereMover3;
	public SaverScriptAndUnityAds saverScriptAndUnityAds66;
	// Use this for initialization
	void Start () {
		sphereMover2 = GameObject.Find ("Sphere");
		cameraController2 = GameObject.Find ("Main Camera");
		sphereMover3=GameObject.Find ("Sphere").GetComponent<SphereMover> ();
		saverScriptAndUnityAds66 =GameObject.Find ("SaverAndUnityAds").GetComponent<SaverScriptAndUnityAds> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (sphereMover2.transform.position.z <= cameraController2.transform.position.z) {
			sphereMover3.tapForRestart.SetActive (true);
			sphereMover3.tapForRestart1.SetActive (true);
		}
		if(Insta){
			zAxis = zAxis + moreZCircle;
			Instantiate (cerchiodaspawn, new Vector3 (-27, 4, zAxis), Quaternion.identity);
			passedText = GameObject.Find ("PassedText").GetComponent<Text> ();
			PassedInt++;
			passedText.text = "" + PassedInt;
			Insta = false;
	}
}
	public void TapForRestart()
	{
		saverScriptAndUnityAds66.liveRecord = 0;
		SceneManager.LoadScene ("Gioco");

	}
	public void BackButton1()
	{
		SceneManager.LoadScene ("Menu");
	}
}