﻿using UnityEngine;
using System.Collections;

public class musicPlayerScript : MonoBehaviour {


	public static musicPlayerScript  musicplayerScript;
	public float highPitch;
	public float lowPitch;
	public float pitch1;
	public AudioSource audio1;
	public float startingpitch;
	public float time;

	public float smoother;
		

	void Awake()
	{
		if (musicplayerScript == null) {
			DontDestroyOnLoad (gameObject);
			musicplayerScript = this;
		} else if (musicplayerScript != this)
			Destroy (gameObject);
		audio1 = GetComponent<AudioSource> ();

		pitch1 = startingpitch;
		}
	void Update()
	{
		
		time += Time.deltaTime;
		if (time >= 16f && time <= 40f) 
		{
			
			pitch1 = lowPitch;

		}
		else if (time>=41f && time <= 65f)
		{
			pitch1 = highPitch;

		}
		else if (time>=66f )
		{
			pitch1 = startingpitch;
			time = 0f;

		}


		audio1.pitch = Mathf.Lerp (audio1.pitch, pitch1,Time.deltaTime * smoother);
	}

	}

	
