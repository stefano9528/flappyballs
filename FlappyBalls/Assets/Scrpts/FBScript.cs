﻿using UnityEngine;
using System.Collections;
//using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class FBScript : MonoBehaviour {

/*	public static FBScript fbScript;
	public GameObject DialogLoggedIn;
	public GameObject DialogLoggedOut;
	public GameObject DialogUsername;
	public GameObject DialogProfilePic;
	public bool gianni=true;
	public bool hulop;
	public Button shareButton;
	public Button inviteButton;
	public Button InviteToChallengeButton;

	void Awake()
	{
		if (fbScript == null) {
			DontDestroyOnLoad (gameObject);
			fbScript = this;
		} else if (fbScript != this)
			Destroy (gameObject);
		if (!FB.IsInitialized) {


			FB.Init (SetInit, OnHideUnity);

		}
	}
	void Start ()
	{
		hulop = true;

	}
	void Update()
	{
		if (SceneManager.GetActiveScene ().name == "Menu" && gianni==true) 
		{
			
			DialogLoggedIn = GameObject.Find ("FBLoggedIn");
			DialogLoggedOut = GameObject.Find ("FBLoggedOUT");
			DialogUsername = GameObject.Find ("Username");
			DialogProfilePic= GameObject.Find ("ProfilePic");
			shareButton = GameObject.Find ("ShareButtonFB").GetComponent<Button>();
			shareButton.onClick.AddListener (() => Share ());
			inviteButton= GameObject.Find ("InviteButtonFB").GetComponent<Button>();
			inviteButton.onClick.AddListener (() => Invite ());
			InviteToChallengeButton= GameObject.Find ("InviteToFight").GetComponent<Button>();
			InviteToChallengeButton.onClick.AddListener (() => Invite ());
			if(hulop){
			DealWithFBMenus (FB.IsLoggedIn);
				hulop = false;
			}
			gianni = false;

		}
		if (SceneManager.GetActiveScene ().name == "Gioco") 
		{
			gianni = true;
			hulop = true;
		}
	}

	void SetInit()
	{

		if (FB.IsLoggedIn) {
			Debug.Log ("FB is logged in");
		} else {
			Debug.Log ("FB is not logged in");
		}

		//DealWithFBMenus (FB.IsLoggedIn);

	}

	void OnHideUnity(bool isGameShown)
	{

		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}

	}

	public void FBlogin()
	{

		List<string> permissions = new List<string> ();
		permissions.Add ("public_profile");

		FB.LogInWithReadPermissions (permissions, AuthCallBack);
	}

	void AuthCallBack(IResult result)
	{

		if (result.Error != null) {
			Debug.Log (result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB is logged in");
			} else {
				Debug.Log ("FB is not logged in");
			}

			DealWithFBMenus (FB.IsLoggedIn);
		}

	}

     void DealWithFBMenus(bool isLoggedIn)
	{

		if (isLoggedIn) {
			DialogLoggedIn.SetActive (true);
			DialogLoggedOut.SetActive (false);

			FB.API ("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
			FB.API ("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);

		} else {
			DialogLoggedIn.SetActive (false);
			DialogLoggedOut.SetActive (true);
		}

	}

	void DisplayUsername(IResult result)
	{

		Text UserName = DialogUsername.GetComponent<Text> ();

		if (result.Error == null) {

			UserName.text = "Hi there, " + result.ResultDictionary ["first_name"];

		} else {
			Debug.Log (result.Error);
		}

	}

	void DisplayProfilePic(IGraphResult result)
	{

		if (result.Texture != null) {

			Image ProfilePic = DialogProfilePic.GetComponent<Image> ();

			ProfilePic.sprite = Sprite.Create (result.Texture, new Rect (0, 0, 128, 128), new Vector2 ());

		}

	}
	public void LoginWithPublishPermissions()
	{

	List<string> permissions = new List<string> ();
		permissions.Add ("publish_actions");
		FB.LogInWithPublishPermissions (permissions, AuthCallBack);
	}
	public void Share()
	{
		FB.FeedShare (
			string.Empty,
			new Uri("http://linktoga.me"),
			"Hello this is the title",
			"This is the caption",
			"Check out this game",
			new Uri("https://i.ytimg.com/vi/NtgtMQwr3Ko/maxresdefault.jpg"),
			string.Empty,
			ShareCallback
		);
	}

	void ShareCallback(IResult result)
	{
		if (result.Cancelled) {
			Debug.Log ("Share Cancelled");
		} else if (!string.IsNullOrEmpty (result.Error)) {
			Debug.Log ("Error on share!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			Debug.Log ("Success on share");
		}
	}
	public void Invite()
	{
		FB.Mobile.AppInvite (
			new Uri("http://linktoga.me"),
			new Uri("https://i.ytimg.com/vi/NtgtMQwr3Ko/maxresdefault.jpg"),
			InviteCallback
		);
	}

	void InviteCallback(IResult result)
	{
		if (result.Cancelled) {
			Debug.Log ("Invite Cancelled");
		} else if (!string.IsNullOrEmpty (result.Error)) {
			Debug.Log ("Error on invite!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			Debug.Log ("Success on Invite");
		}
	}
	public void InviteToFight()
	{
		FB.AppRequest ("Come and join me, I bet you can't beat my score!", null, new List<object> (){ "app_users" }, null, null, null, null, InviteToFightCallBack);

	}
	void InviteToFightCallBack(IAppRequestResult result)
	{
		if (result.Cancelled) {
			Debug.Log ("Invite Cancelled");
		} else if (!string.IsNullOrEmpty (result.Error)) {
			Debug.Log ("Error on invite!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			Debug.Log ("Success on Invite");
		}
	}
	/*public void SetScore()
	{
		var ScoreData = new Dictionary<string,string> ();
		ScoreData ["score"] = .ToString();
		FB.API ("/me/scores", Facebook.HttpMethod.POST, delegate(I) {
			Debug.Log ("Score submit result: " + result.Text);
		}, ScoreData);


	}
	public void QueryScores()
	{
		FB.API ("/app/scores?fields=score,user.limit(30)", Facebook.HttpMethod.GET, ScoresCallBack);

	}*/
}
