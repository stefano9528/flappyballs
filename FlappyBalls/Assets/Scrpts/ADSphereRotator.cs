﻿using UnityEngine;
using System.Collections;

public class ADSphereRotator : MonoBehaviour {

	public float adsSphereSpeed;
	public EncryptingScript encryptingscript5;

	public Renderer rend;
	public string jiko;
	private string juko="jimny";

	// Use this for initialization
	void Start () {
		encryptingscript5 = GameObject.Find ("EncrypterObject").GetComponent<EncryptingScript> ();
		rend = GetComponent<Renderer>();
		rend.material.SetColor ("_EmissionColor", Color.red);
		jiko = encryptingscript5.Decrypt (PlayerPrefs.GetString ("RecordSaved"));
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up * adsSphereSpeed);
		if(jiko==juko){


			rend.material.SetColor ("_EmissionColor", Color.green);
	}
}
}