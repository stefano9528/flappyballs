﻿using UnityEngine;
using System.Collections;

public class MenuBackGroundRotator : MonoBehaviour {

	public float backGroundRotationSpeedMenu;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.forward * backGroundRotationSpeedMenu);
	}


}
